using Core.DB.Repos;

namespace DemoKotdusov.Utils;

public static class GlobalContext
{
    public static MaterialRepo MaterialRepo { get; set; }
    public static MaterialTypeRepo MaterialTypeRepo { get; set; }
    public static ProductRepo ProductRepo { get; set; }
    public static ProductTypeRepo ProductTypeRepo { get; set; }
    public static ProductMaterialRepo ProductMaterialRepo { get; set; }
}