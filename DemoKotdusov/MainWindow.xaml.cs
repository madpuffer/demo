﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Core.DB.Context;
using Core.DB.Repos;
using DemoKotdusov.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace DemoKotdusov
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class ProductListModel
        {
            public int Id { get; set; }
            public string ProductType { get; set; }
            public string ProductName { get; set; }
            public decimal Price { get; set; }
            public string Articul { get; set; }
            public string Materials { get; set; }
            public byte[] Image { get; set; }
        }

        private const string DefaultOrder = "По умолчанию";
        private const string AscendingOrder = "От А до Я";
        private const string DescendingOrder = "От Я до А";
        private const string UniversalFilter = "Все";

        private const string FilterKey = "filter";
        private const string SearchKey = "search";
        private const string SortKey = "sort";

        private const int ProductsPerPage = 4;
        private int _lastPageNum = 0;

        private const string SearchPlaceholder = "Введите для поиска";
        
        private List<ProductListModel> _products = new();
        private List<ProductListModel> _paginatedProducts = new();
        
        private Dictionary<string, Action> _listModifications = new ();
        private int _currentPage = 0;
        private int _currentPagesStack = 1;

        public MainWindow()
        {
            InitializeComponent();
            
            Init();

            var prodTypes = GlobalContext.ProductTypeRepo.GetAll();
            var materials = GlobalContext.MaterialRepo.GetAll();
            var receipts = GlobalContext.ProductMaterialRepo.GetAll();

            var products = GlobalContext.ProductRepo.GetAll();
            foreach (var prod in products)
            {
                var materialsId = receipts.Where(x => x.ProductId == prod.Id).Select(x => x.MaterialId);

                _products.Add(new ProductListModel()
                {
                    Id = prod.Id,
                    ProductName = prod.Name,
                    Articul = prod.Articul,
                    Price = prod.MinimalCost,
                    Image = prod.Image,
                    ProductType = prodTypes.First(z => z.Id == prod.ProductTypeId).Name,
                    Materials = string.Join(", ", materials.Where(z => materialsId.Contains(z.Id)).Select(x => x.Name))
                });
            }
            
            _lastPageNum = (int)_products.Count / 4;

            // CbFilter.ItemsSource = GlobalContext.MaterialTypeRepo.GetAll().Select(x => x.Name).ToList();
            SortComboBox.ItemsSource = new[]
            {
                DefaultOrder,
                AscendingOrder,
                DescendingOrder
            };

            SortComboBox.SelectedItem = DefaultOrder;

            var filters = new List<string>()
            {
                UniversalFilter
            };
            
            filters.AddRange(prodTypes.Select(x => x.Name).ToList());

            FilterComboBox.ItemsSource = filters;

            FilterComboBox.SelectedItem = UniversalFilter;

            _paginatedProducts = _products.Skip(_currentPage * ProductsPerPage).Take(ProductsPerPage).ToList();
            
            LvProducts.ItemsSource = _paginatedProducts;
        }

        private void Init()
        {
            var dbContext = new DatabaseContext(new DbContextOptions<DatabaseContext>());

            GlobalContext.MaterialRepo = new MaterialRepo(dbContext);
            GlobalContext.MaterialTypeRepo = new MaterialTypeRepo(dbContext);
            GlobalContext.ProductRepo = new ProductRepo(dbContext);
            GlobalContext.ProductTypeRepo = new ProductTypeRepo(dbContext);
            GlobalContext.ProductMaterialRepo = new ProductMaterialRepo(dbContext);
        }
        
        private void Tb_Search_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox currentTb = sender as TextBox;
            string text = currentTb.Text;

            _listModifications.Remove(SearchKey);
            if (!text.IsNullOrEmpty() && text != SearchPlaceholder)
            {
                _listModifications.Add(SearchKey, () => LvProducts.ItemsSource = ListModifications.Search(LvProducts.ItemsSource.Cast<ProductListModel>(), text));   
            }

            ApplyModifications();
        }

        private void Tb_Search_OnLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox currentTb = sender as TextBox;
            string text = currentTb.Text;

            if (text.IsNullOrEmpty())
            {
                SearchTextBox.Text = SearchPlaceholder;   
                _listModifications.Remove(SearchKey);
            }
            
            ApplyModifications();
        }

        private void OnSortingChanged(object sender, SelectionChangedEventArgs e)
        {
            string order = (sender as ComboBox).SelectedItem as string;

            _listModifications.Remove(SortKey);
            
            if (order != DefaultOrder)
            {
                _listModifications.Add(SortKey, () => LvProducts.ItemsSource = ListModifications.Order(LvProducts.ItemsSource.Cast<ProductListModel>(), order));
            }

            ApplyModifications();
        }

        private void OnFilterChanged(object sender, SelectionChangedEventArgs e)
        {
            var filter = (sender as ComboBox).SelectedItem as string;

            _listModifications.Remove(FilterKey);
            
            if (filter != UniversalFilter)
            {
                _listModifications.Add(FilterKey, () => LvProducts.ItemsSource = ListModifications.FilterByType(LvProducts.ItemsSource.Cast<ProductListModel>(), filter));
            }

            ApplyModifications();
        }

        private void ApplyModifications()
        {
            if (LvProducts is null)
            {
                return;
            }
            
            LvProducts.ItemsSource = _paginatedProducts;

            foreach (var key in _listModifications.Keys)
            {
                _listModifications[key]();
            }
            
            LvProducts.Items.Refresh();
        }

        private void OnGotFocus(object sender, RoutedEventArgs e)
        {
            SearchTextBox.Text = "";
        }

        private void OnNextPagesClick(object sender, RoutedEventArgs e)
        {
            var start = int.Parse(FourNumButton.Content.ToString());

            if (start >= _lastPageNum)
            {
                return;
            }

            FirstNumButton.Content = start + 1;
            SecNumButton.Content = start + 2;
            ThirdNumButton.Content = start + 3;
            FourNumButton.Content = start + 4;
        }

        private void OnPreviousPagesClick(object sender, RoutedEventArgs e)
        {
            var start = int.Parse(FourNumButton.Content.ToString());

            if (start == 4)
            {
                return;
            }
            
            FirstNumButton.Content = int.Parse(FirstNumButton.Content.ToString()) - 4;
            SecNumButton.Content = int.Parse(SecNumButton.Content.ToString()) - 4;
            ThirdNumButton.Content = int.Parse(ThirdNumButton.Content.ToString()) - 4;
            FourNumButton.Content = int.Parse(FourNumButton.Content.ToString()) - 4;
        }

        private void OnPageClick(object sender, RoutedEventArgs e)
        {
            var page = int.Parse((sender as Button).Content.ToString());

            if (page > _lastPageNum)
            {
                return;
            }

            _currentPage = page - 1;
            
            _paginatedProducts = _products.Skip(_currentPage * ProductsPerPage).Take(ProductsPerPage).ToList();
            ApplyModifications();
        }
    }
}