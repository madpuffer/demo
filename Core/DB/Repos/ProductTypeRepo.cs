using Core.DB.Context;
using Core.DB.Entities;
using Core.DB.Repos.Base;

namespace Core.DB.Repos;

public class ProductTypeRepo : BaseRepo<ProductType>
{
    public ProductTypeRepo(DatabaseContext context) : base(context)
    {
    }
}