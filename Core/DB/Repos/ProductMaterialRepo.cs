using Core.DB.Context;
using Core.DB.Entities;
using Core.DB.Repos.Base;

namespace Core.DB.Repos;

public class ProductMaterialRepo : BaseRepo<ProductMaterial>
{
    public ProductMaterialRepo(DatabaseContext context) : base(context)
    {
    }
}