using Core.DB.Context;
using Core.DB.Entities;
using Core.DB.Repos.Base;

namespace Core.DB.Repos;

public class ProductRepo : BaseRepo<Product>
{
    public ProductRepo(DatabaseContext context) : base(context)
    {
    }
}