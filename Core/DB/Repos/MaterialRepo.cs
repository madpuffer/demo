using Core.DB.Context;
using Core.DB.Entities;
using Core.DB.Repos.Base;

namespace Core.DB.Repos;

public class MaterialRepo : BaseRepo<Material>
{
    public MaterialRepo(DatabaseContext context) : base(context)
    {
    }
}