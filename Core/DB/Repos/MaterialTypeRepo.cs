using Core.DB.Context;
using Core.DB.Entities;
using Core.DB.Repos.Base;

namespace Core.DB.Repos;

public class MaterialTypeRepo : BaseRepo<MaterialType>
{
    public MaterialTypeRepo(DatabaseContext context) : base(context)
    {
    }
}