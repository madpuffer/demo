using System.ComponentModel.DataAnnotations.Schema;
using Core.DB.Entities.Base;

namespace Core.DB.Entities;

[Table("Material")]
public class Material : BaseEntity
{
    public string Name { get; set; }
    public int MaterialTypeId { get; set; }
}