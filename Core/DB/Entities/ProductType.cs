using System.ComponentModel.DataAnnotations.Schema;
using Core.DB.Entities.Base;

namespace Core.DB.Entities;

[Table("ProductType")]
public class ProductType : BaseEntity
{
    public string Name { get; set; }
}