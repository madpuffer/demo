using System.ComponentModel.DataAnnotations.Schema;
using Core.DB.Entities.Base;

namespace Core.DB.Entities;

[Table("Product")]
public class Product : BaseEntity
{
    public string Name { get; set; }
    public string Articul { get; set; }
    public decimal MinimalCost { get; set; }
    public int ProductTypeId { get; set; }
    public byte[]? Image { get; set; }
}