using System.ComponentModel.DataAnnotations.Schema;
using Core.DB.Entities.Base;

namespace Core.DB.Entities;

[Table("MaterialType")]
public class MaterialType : BaseEntity
{
    public string Name { get; set; }
}