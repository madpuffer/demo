using System.ComponentModel.DataAnnotations.Schema;
using Core.DB.Entities.Base;

namespace Core.DB.Entities;

[Table("ProductMaterial")]
public class ProductMaterial : BaseEntity
{
    public int MaterialId { get; set; }
    public int ProductId { get; set; }
}