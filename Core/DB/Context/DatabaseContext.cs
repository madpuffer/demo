using Core.DB.Entities;
using Microsoft.EntityFrameworkCore;

namespace Core.DB.Context;

public class DatabaseContext : DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options): base(options) {}
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder
            .UseSqlServer(@"data source=DESKTOP-C665MB9\MSSQLSERVER02;Database=DemoKotdusov;Integrated Security=True;Trusted_Connection=True;TrustServerCertificate=True")
            .UseCamelCaseNamingConvention();
    }
    
    public DbSet<Material> Materials { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<MaterialType> MaterialTypes { get; set; }
    public DbSet<ProductType> ProductTypes { get; set; }
    public DbSet<ProductMaterial> ProductMaterials { get; set; }
}